#include<stdio.h>
void add(int *a,int *b,int *s);
void div(int *a,int *b,int *d);
void rem(int *a,int *b,int *r);
int main()
{
 int n1,n2,sum,di,r;
 printf("enter 2 numbers\n");
 scanf("%d%d",&n1,&n2);
 add(&n1,&n2,&sum);
 printf("sum of 2 numbers is %d\n",sum);
 div(&n1,&n2,&di);
 printf("division of 2 numbers is %d\n",di);
 rem(&n1,&n2,&r);
 printf("remainder of 2 numbers is %d\n",r);
 return 0;
}

void add(int *a,int *b,int *s)
{
    *s=*a+*b;
}
void div(int *a,int *b,int *di)
{
    *di=(*a)/(*b);
}
void rem(int *a,int *b,int *r)
{
    *r=(*a)%(*b);
}
