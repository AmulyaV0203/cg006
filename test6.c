
#include <stdio.h>

int input_n()
{
    int a;
    printf("Enter a number\n");
    scanf("%d",&a);
    return a;
}

void input_array(int n, int a[n])
{
    for(int i=0;i<n;i++){
        printf("Enter the element no %d of the array\n",i);
        scanf("%d",&a[i]);
    }
}

int find_array_avg(int n, int a[n])
{
    int sum=0,avg;
    for(int i=0;i<n;i++) {
        sum += a[i];
        avg=sum/n;
    }
    return avg;
}

int show_results(int n, int a[n], int avg)
{
    int i;
    printf("The avg of n numbers is/n"); 
    for(i=0;i<n;i++) 
{
        printf("%d+",a[i]);
    printf("%d=%d",a[i],avg);
}
}

int main()
{
    int n,avg;
    n = input_n();
    int a[n];
    input_array(n,a);
    avg=find_array_avg(n,a);
    show_results(n,a,avg);
}
