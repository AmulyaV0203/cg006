#include<stdio.h>
void swap(int *n1,int *n2)
{
 int temp;
 temp=*n1;
 *n1=*n2;
 *n2=temp;
}
int main()
{
 int n1=25,n2=44;
 printf("before swapping:");
 printf("\n n1 value is %d",n1);
 printf("\n n2 value is %d",n2);
 
 swap(&n1,&n2);
 printf("\nafter swapping:");
 printf("\n n1 value is %d",n1);
 printf("\n n2 value is %d",n2);
 return 0;
}